/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['media.tenor.com', 'cdn.discordapp.com', 'media1.tenor.com', 'tenor.com'],
  },
};

module.exports = nextConfig;
