import type { Metadata } from 'next';
import './globals.css';

export const metadata: Metadata = {
  title: 'Supriyo Mal',
  description: ' Hi, I’m @Supriyo I\'m currently learning Rust and I am currently living in West Bengal, India.',
  themeColor: '#8B5CF6',
  robots: 'index, follow',
  viewport: 'width=device-width, initial-scale=1, maximum-scale=1',
  icons: 'https://avatars.githubusercontent.com/u/72860690?s=400&u=4bd6c714de72792a6747b2e799cbd1b031034f6c&v=4',
  openGraph: {
    images: 'https://avatars.githubusercontent.com/u/72860690?s=400&u=4bd6c714de72792a6747b2e799cbd1b031034f6c&v=4',
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
