import { IconType } from 'react-icons';
import { BsDiscord, BsFillFilePdfFill, BsGithub, BsInstagram, BsLinkedin, BsTwitterX } from 'react-icons/bs';
import { DiGo, DiJavascript1, DiNodejs, DiReact} from 'react-icons/di';
import { SiExpress, SiMongodb, SiTypescript } from 'react-icons/si';
import { TbBrandRust } from 'react-icons/tb';

export const projects: { name: string; description: string; iconURL: string; link: string; }[] = [
  {
    name: 'BookinGo',
    description: 'Find budget-friendly hotels for your dream vacation.',
    link: 'https://github.com/Supsource/BookinGo',
    iconURL: 'https://cdn.discordapp.com/icons/1147505872625483819/71eca3384e61703feeba4448a633d5ca.png',
  },
  {
    name: 'Umm, Social Media App',
    description: 'Developed a cross-platform social media application using React Native, JavaScript, Node.js, MongoDB, Express.js, and Firebase.',
    link: 'https://discord.gg/altyapilar',
    iconURL: 'https://cdn.discordapp.com/icons/1096085223881576549/c2a37851263289188afde2ea135e0665.png',
  },
  {
    name: 'VidKaller',
    description: 'It allows you to view local news via Discord.',
    link: 'https://bit.ly/haberlerbot',
    iconURL: 'https://cdn.discordapp.com/icons/1147505872625483819/71eca3384e61703feeba4448a633d5ca.png',
  },
  {
    name: 'Chatter',
    description: 'It provides access to free projects and codes.',
    link: 'https://discord.gg/altyapilar',
    iconURL: 'https://cdn.discordapp.com/icons/1096085223881576549/c2a37851263289188afde2ea135e0665.png',
  },
];

export const githubUsername = 'Supsource';
export const discordId = '@supriyo_dev';

export const socialMediaAccounts: { name: string; link: string; iconURL: IconType }[] = [
  {
    name: 'Github',
    link: 'https://github.com/' + githubUsername,
    iconURL: BsGithub,
  },
  {
    name: 'Discord',
    link: 'https://discord.com/' + discordId,
    iconURL: BsDiscord,
  },
  {
    name: 'Instagram',
    link: 'https://www.instagram.com/iam_ya7',
    iconURL: BsInstagram,
  },
  {
    name: 'X',
    link: 'https://twitter.com/iamsupriyo7',
    iconURL: BsTwitterX,
  },
  {
    name: 'Linkledln',
    link: 'https://twitter.com/iamsupriyo7',
    iconURL: BsLinkedin,
  },
];


// my changes
export const socialMediaAccounts2: { name: string; link: string; iconURL: IconType }[] = [
  {
    name: 'CV',
    link: 'https://github.com/' + githubUsername,
    iconURL: BsFillFilePdfFill,
  },
];


export const technologiesAndLanguages: { name: string; iconURL: IconType }[] = [
  {
    name: 'Typescript',
    iconURL: SiTypescript,
  },
  {
    name: 'React.js',
    iconURL: DiReact,
  },
  {
    name: 'Golang',
    iconURL: DiGo,
  },
  {
    name: 'Node.JS',
    iconURL: DiNodejs,
  },
  {
    name: 'Rust',
    iconURL: TbBrandRust,
  },
  {
    name: 'MongoDB',
    iconURL: SiMongodb,
  },
  {
    name: 'Express',
    iconURL: SiExpress,
  },
  {
    name: 'JavaScript',
    iconURL: DiJavascript1,
  },
];
